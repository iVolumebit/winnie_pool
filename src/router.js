import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home-page.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/stats',
      name: 'stats',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "Stats-page" */ './views/Stats-page.vue')
    },
    {
      path: '/faq',
      name: 'faq',
      component: () => import(/* webpackChunkName: "Faq-page" */ './views/Faq-page.vue')
    }
  ],
  mode: 'history',
  scrollBehavior(from, to, savedPosition) {

    if(!to.hash){
      return {
        x: 0,
        y: 0
      }
    }
  }
})
