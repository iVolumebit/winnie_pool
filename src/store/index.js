import Vue from 'vue';
import Vuex from 'vuex'
Vue.use(Vuex);


export default new Vuex.Store({
  state: {
    isLogin: localStorage.login || false,
    userName: localStorage.login,
    overal: {}
  },
  mutations: {
    setOveral(state, payload) {
      state.overal = payload
    }
  },
  actions: {
    asyncOveral ( { commit }, payload ) {
      commit('setOveral', payload);
    }
  }
})